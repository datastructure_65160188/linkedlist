/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linklist1;

/**
 *
 * @author Tauru
 */
class LinkListApp {
    public static void main(String[] args) {
        LinkList theList = new LinkList(); // Create a new list
        theList.insertFirst(22, 2.99); // Insert four items
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);

        theList.displayList(); // Display the list

        while (!theList.isEmpty()) { // Until it's empty
            Link aLink = theList.deleteFirst(); // Delete the first link
            System.out.print("Deleted "); // Display it
            aLink.displayLink();
            System.out.println("");
        }

        theList.displayList(); // Display the list again
    } // end main()
} // end class LinkListApp
